export const state = () => ({
  email: '',
  password: '',
  username: '',
  accessToken: null,
  id: '',
})

export const mutations = {
  update(state, data) {
    Object.assign(state, data)

    if (process.browser) {
      localStorage.setItem('user', JSON.stringify(data))
    }

  },
}

export const getters = {
  email: (state) => state.email,
  username: (state) => state.username,
  accessToken: (state) => state.accessToken,
  id: (state) => state.id,
  isLogged: (state) => !!state.accessToken,
}

export const actions = {
  signup({ dispatch, commit }, data) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/register', data).then(
        (response) => {
          if (response.status === 201) {
            const { email, username } = data
            commit('update', {
              email,
              username,
              ...response.data,
            })

            dispatch('user')
              .then((status) => {
                resolve(status)
              })
              .catch(reject)
          } else {
            reject(
              new Error(`Response status was ${response.status}, expected 201`)
            )
          }
        },
        (error) => reject(error)
      )
    })
  },
  login({ dispatch, commit }, credentials) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/login', credentials).then((response) => {
        if (response.status === 200) {
          const { email } = credentials

          commit('update', { email, ...response.data })

          dispatch('user')
            .then((status) => {
              resolve(status)
            })
            .catch(reject)
        } else {
          reject(
            new Error(`Response status was ${response.status}, expected 200`)
          )
        }
      }, reject)
    })
  },

  user({ commit, state }) {
    return new Promise((resolve, reject) => {
      this.$axios.get(`/users?email=${state.email}`).then(
        (response) => {
          if (response.status === 200 || response.status === 304) {
            const { email, username, id } = response.data[0]
            commit('update', { ...state, email, username, id })
            resolve(response.status)
          }
          reject(
            new Error(
              `Response status was ${response.status}, expected 200/304`
            )
          )
        },
        (error) => reject(error)
      )
    })
  },

  loadFromLocalStorage({ commit }, userInfo) {
    commit('update', userInfo)
  }

}
