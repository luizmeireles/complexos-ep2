
export default function ({ route, store, redirect }) {

  if (process.browser) {
    const user = JSON.parse(localStorage.getItem('user'))
    if (user && user.accessToken) {
      store.dispatch('loadFromLocalStorage', user)
    }
  }

  if (store.getters.isLogged) {

    if (route.name === 'index')
      redirect(`/users/${store.getters.id}`)
    else if (route.name === 'users-id' && store.getters.id !== parseInt(route.params.id)) {

      redirect('/log-in')

    }
  }

  if ((route.name === 'users-id' || route.name === 'index') && !store.getters.isLogged) {
    redirect('/log-in')
  }

}
