export default function ({ $axios, store }) {
  $axios.onRequest(() => {
    if (store.state.accessToken) {
      $axios.setToken(store.state.accessToken, 'Bearer')
    }
  })
}
